package com.example.gestiondefacturas;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Captura_Imagen extends AppCompatActivity  implements View.OnClickListener {


    private final int REQUEST_PERMISSION_STORAGE_SAVE = 101;
    private final int REQUEST_PERMISSION_STORAGE_DELETE = 102;

    static  final  int REQUEST_IMAGE_CATURE =1;

    Bitmap imageBitmap;

    private final String FOLDER_NAME = "UOCImagenApp";
    private final String FILE_NAME = "imageapp.jpg";

    private Button buttonOpenImage;
    private ImageView imageView;
    private TextView textViewMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captura_imagen);

        buttonOpenImage = findViewById(R.id.image_app_btn_capture);
        imageView =findViewById(R.id.image_app_iv_picture);
        textViewMessage = findViewById(R.id.image_app_tv_message);

        buttonOpenImage.setOnClickListener(this);

        cargarImagenSiExiste();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.opcionLogin) {
            // onDeleteMenuTap();
            return true;

        } else if (item.getItemId() == R.id.opcionRegistrar) {
            // onSaveMenuTap();
        }
        return super.onOptionsItemSelected(item);
    }


    private  void cargarImagenSiExiste(){
        String myImage = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/"+ FOLDER_NAME +"/"+FILE_NAME;
        File imagen = new File(myImage);
        if (imagen.exists()){
            Picasso.get().load(imagen).into(imageView);
            textViewMessage.setVisibility(View.INVISIBLE);
        }
        else {
            textViewMessage.setVisibility(View.VISIBLE);
        }
    }

    private  boolean hasPermissionsToWrite(){
        return ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onClick(View v) {
        if(v == buttonOpenImage){
            dispatchTakePictureIntent();
        }

    }

    private  void  dispatchTakePictureIntent(){
        Intent taKePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (taKePictureIntent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(taKePictureIntent, REQUEST_IMAGE_CATURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CATURE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
            textViewMessage.setVisibility(View.INVISIBLE);
        }
    }
    private boolean hasPermissionsTowrite(){
        return  ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }
    private  void onSaveMenuTap(){
        if (!hasPermissionsToWrite()) {
            ActivityCompat.requestPermissions(
                    this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_PERMISSION_STORAGE_SAVE);
        }else {
            if (imageView.getDrawable()!= null){
                createFolder();
                String storagerDir = Environment.getExternalStorageDirectory()+"/UOCImageApp/";
                createImagenFile(storagerDir, this.FILE_NAME, imageBitmap);

            } else {
                Toast.makeText(this, "Tome una foto primero!",Toast.LENGTH_LONG).show();
            }
        }
    }
    private void  createFolder(){
        String myfolder = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+ FOLDER_NAME;

        File folder = new File(myfolder);
        if(!folder.exists())
            if (!folder.mkdir()){
                Toast.makeText(this, FOLDER_NAME+"no se puede crear ", Toast.LENGTH_LONG).show();

            }
            else
                Toast.makeText(this, FOLDER_NAME+" creada exitosamente", Toast.LENGTH_LONG).show();
    }
    private void createImagenFile(String storageDir, String fileName, Bitmap bitmap){
        try{
            File myFile = new File(storageDir, fileName);
            FileOutputStream stream = new FileOutputStream(myFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();

            Toast.makeText(this,"imagen guardada!", Toast.LENGTH_LONG).show();

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults) {
        switch (requestCode){
            case REQUEST_PERMISSION_STORAGE_DELETE:{
                if (grantResults. length > 0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){

                }else {
                    Toast.makeText(this,"delete permission revoke", Toast.LENGTH_LONG).show();
                }
            }
            case REQUEST_PERMISSION_STORAGE_SAVE:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    createFolder();
                    String storageDir = Environment.getExternalStorageDirectory()+"/UOCImageApp/";
                    createImagenFile(storageDir, FILE_NAME, imageBitmap);
                }else {
                    Toast.makeText(this,"permiso de guardar denegado", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
}
